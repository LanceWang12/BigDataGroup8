import pandas as pd
import time
from sklearn.model_selection import train_test_split


import os
import argparse
import torch
import torch.optim as optim
import torch.nn as nn
from transformers import RobertaTokenizerFast

from header.model.utils import fix_seed, load_data, EarlyStopping
from header.model import TweetRunner
from header.model.BERT import SentimentRoberta
from header.preprocessing import TweetDataset


def parse() -> argparse.Namespace:
    parser = argparse.ArgumentParser()
    parser.add_argument('--load', type = str, default = None)
    parser.add_argument('--epochs', type = int, default = 10)
    parser.add_argument('--batch_size', type = int, default = 48)
    parser.add_argument('--lr', type = float, default = 1e-3)
    parser.add_argument('--trainable', action='store_true', default = True)
    parser.add_argument('--device', type = str, default = 'cuda:3')
    parser.add_argument('--nclass', type = int, default = 3)
    parser.add_argument('--patience', type = int, default = 3)
    parser.add_argument('--delta', type = float, default = 1e-2)
    parser.add_argument('--freeze', type = bool, default = True)
    parser.add_argument('--dropout', type = float, default = 0.3)
    args = parser.parse_args()

    print('=' * 70)
    for key, value in vars(args).items():
        print(f'{key}: {value}')
    print('=' * 70)

    return args


def main() -> None:
    args = parse()
    
    # -------- Prepare dataset --------
    df = pd.read_csv('./data/tweet/tweet_pretrained_dataset.csv')

    train, test = train_test_split(df, test_size = 0.25, random_state = 0)
    train.reset_index(drop = True, inplace = True)
    test.reset_index(drop = True, inplace = True)
    
    # prepare tokenizer
    PRE_TRAINED_MODEL_NAME = 'roberta-base'
    tokenizer = RobertaTokenizerFast.from_pretrained(PRE_TRAINED_MODEL_NAME)
    
    # some hyperparameter for tokenizer
    seq_len = 160
    
    trainset = TweetDataset(
        train['text'], train['target'], tokenizer, seq_len
    )
    
    testset = TweetDataset(
        test['text'], test['target'], tokenizer, seq_len
    )
    
    train_loader = load_data(trainset, batch_size = args.batch_size, shuffle = True)
    test_loader = load_data(testset, batch_size = args.batch_size, shuffle = False)
    
    # -------- Prepare model --------
    model = SentimentRoberta(n_class = 3, dropout = 0.3)

    # model freezen for transfer learning
    if args.freeze:
        model.freeze()


    if args.load:
        model.load_state_dict(torch.load(args.load))

    optimizer = optim.Adam(model.parameters(), lr = args.lr)

    criterion = nn.CrossEntropyLoss()
    scheduler = optim.lr_scheduler.CosineAnnealingLR(optimizer, T_max = 5, eta_min = 1e-7)
    
    model_ckpt = [
        EarlyStopping(
            patience = args.patience,
            verbose = True,
            delta = args.patience,
            path = './ckpt/Roberta/best_ckpt.pt'
        )
    ]

    runner = TweetRunner(model, optimizer, criterion, model_ckpt[0], args.device)

    if args.trainable:
        print(f'Start to train...\n')
        start = time.time()
        
        #try:
        runner.train(
            args.epochs, train_loader, 
            valid_loader = test_loader, scheduler = scheduler
        )
        #except:
            #os.makedirs('ckpt/Roberta', exist_ok=True)
            #for key, value in runner.weights.items():
               # torch.save(value.state_dict(), os.path.join('ckpt/Roberta/', f'last_ckpt.pt'))
            
        end = time.time()
        print(f'End in {end - start:.4f}s...\n')
        
        os.makedirs('ckpt/Roberta', exist_ok=True)
        for key, value in runner.weights.items():
            torch.save(value.state_dict(), os.path.join('ckpt/Roberta/', f'last_ckpt.pt'))
    
    model.load_state_dict(torch.load('ckpt/Roberta/best_ckpt.pt'))
    runner.test(test_loader)


if __name__ == '__main__':
    fix_seed(seed=0)

    main()
