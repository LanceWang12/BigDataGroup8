#!/usr/bin/env python
# coding: utf-8

from header.model.SCINet import PL_SCINet, SCINet
from header.model.SCINet import * # will import both PL_SCINet and SCINet
from header.evaluate import regression_report, plot_result
#
import os
import torch
import torch.nn as nn
import pandas as pd
import numpy as np
from tqdm import tqdm
import torch.optim as optim
# from sklearn.utils import shuffle
from torch.autograd import Variable
from matplotlib import pyplot as plt
from torch.utils.data import DataLoader
from sklearn.preprocessing import StandardScaler, MinMaxScaler
#
device = torch.device("cuda:0")

#

company_lst = [
    'AAPL', 'AXP',
 'BA',
 'CAT',
 'CSCO',
 'HD',
 'IBM',
 'JNJ',
 'JPM',
 'KO',
 'MCD',
 'MMM',
 'MRK',
 'MSFT',
 'PFE',
 'PG',
 'TRV',
 'V',
 'VZ',
 'WBA',
 'XOM'
]


#
def create_xy_sequences(x,y, input_len, output_len, multiple_targets):
    
    feature_seq = []
    label_seq = []
    L = len(x) # batch_size
    x = pd.DataFrame(x).astype("float")
    y = pd.DataFrame(y).astype("float")
    if multiple_targets:
        for i in range(L-input_len-output_len):
            X_seq = x[i:i+input_len][:]
            Y_seq = y[i+input_len:i+input_len+output_len][:]

            feature_seq.append(X_seq)
            label_seq.append(Y_seq)
    else:
        for i in range(L-input_len-output_len):
            X_seq = x[i:i+input_len]
            Y_seq = y[i+input_len+output_len:i+input_len+output_len+1]
            feature_seq.append(X_seq)
            label_seq.append(X_seq)
            
    # feature_seq = [StandardScaler().fit_transform(X) for X in feature_seq]
    feature_seq = [MinMaxScaler().fit_transform(i) for i in feature_seq]
    X = torch.FloatTensor(np.array(feature_seq))
    Y = torch.squeeze(torch.FloatTensor(np.array(label_seq)))
    # print('X shape:',X.shape)
    # print('y shape:',Y.shape)
    
    return X, Y


#
def train(target='Predict_Close',
          input_len = 16, 
          output_len = 14,
          n_epochs = 600,
          batch_size = 200 ,
          multiple_targets=True
          ):

    for i, company in enumerate(company_lst): #[0:5]
        
        print(f'{company} train start...')
        
        filepath = f'./data/pkl/{company}.pkl'
        df = pd.read_pickle(filepath)

        test_start = pd.to_datetime("2020-01-01")
        train = df[df['Date'] < test_start].reset_index(drop = True)
        test = df[df['Date'] >= test_start].reset_index(drop = True)
        train = train.drop(columns = ['Date'])
        test = test.drop(columns = ['Date'])
        #
        x_train = train.drop(columns = ['Predict_Close','Close'])
        y_train = train['Close']

        y_train = np.asarray(y_train)
        x_train = np.asarray(x_train)
        x_train_tensor = Variable(torch.Tensor(x_train))
        y_train_tensor = Variable(torch.Tensor(y_train))
        
        
        dataset = torch.utils.data.TensorDataset(x_train_tensor, y_train_tensor)
        
        train_loader = DataLoader(dataset=dataset,
                              batch_size=batch_size,
                              num_workers=0,
                              drop_last=True)
    
        model = PL_SCINet(input_len, output_len).to(device)
        optimizer = optim.Adam(model.parameters())
        # Training setup
        
        epoch_error = []
        epoch_loss = []
        hist_error = []
        hist_loss = []
        # Training loop
        for epoch in tqdm(range(n_epochs)):  
            for i_batch, batch in enumerate(train_loader):
                inputs, outputs = batch
                
                inputs, outputs = create_xy_sequences(inputs,
                                                      outputs,
                                                      input_len,
                                                      output_len,
                                                      multiple_targets)
                inputs = inputs.to(device)
                outputs = outputs.to(device)
                optimizer.zero_grad()
                pred = model.forward(inputs)
                loss = nn.L1Loss()(pred, outputs)
                loss.backward()
                optimizer.step()
                #
                pred = pred.cpu()
                outputs = outputs.cpu()
                loss = loss.cpu()
                #
                # error = torch.mean(torch.sqrt((pred-outputs)**2)).detach().numpy()
                error = nn.MSELoss()(pred,outputs).detach().numpy()
                epoch_error.append(error)
                epoch_loss.append(loss.data.detach().numpy())
                
            hist_error.append(np.mean(epoch_error))
            hist_loss.append(np.mean(epoch_loss))
            print("Epoch %d -- MAE %f, MSE %f " % (epoch+1, hist_loss[-1], hist_error[-1]))
        
        # save model
        SAVE_PATH = f"outputs/scinet_{company}.dat"
        torch.save(model.state_dict(), SAVE_PATH)
        print("Model saved to %s" % SAVE_PATH)
        
        # save training loss to csv
        df_loss = pd.DataFrame(
        {'MAE': hist_loss,
          'MSE': hist_error
        })
        df_loss.to_csv(f'./outputs/losses/scinet_loss_{company}.csv')
        
        # plot MAE,MSE curve
        # Plot some training history data
        f, (ax1, ax2) = plt.subplots(2, 1, sharex=True)
        ax1.plot(hist_error)
        ax1.set_ylabel("MSE")
        ax2.plot(hist_loss)
        ax2.set_ylabel("MAE")
        ax2.set_xlabel("Epoch")
        plt.title(f"{company}")
        plt.show()
        plt.savefig(f"./graph/SCINet_loss_{company}.png")

        
###
def test(input_len=16,output_len=14):
    
    benchmark_dict = dict()
    benchmark_dict['mae'] = np.zeros(len(company_lst))
    benchmark_dict['rmse'] = np.zeros(len(company_lst))
    benchmark_dict['mape'] = np.zeros(len(company_lst))
    benchmark_dict['r2'] = np.zeros(len(company_lst))
    
    model = PL_SCINet(input_len, output_len)
    for i, company in enumerate(company_lst):
        print(f'{company} test start...')
        model.load_state_dict(torch.load(f"./outputs/weights/scinet_{company}.dat"))
        filepath = f'./data/pkl/{company}.pkl'
        df = pd.read_pickle(filepath)

        test_start = pd.to_datetime("2020-01-01")
        test = df[df['Date'] >= test_start].reset_index(drop = True)
        test_df = test.drop(columns = ['Date'])
        x_test = np.asarray((test_df.drop(columns = ['Predict_Close','Close'])))
        y_test = np.asarray((test_df['Close']))

        x_test_tensor = Variable(torch.Tensor(x_test))
        y_test_tensor = Variable(torch.Tensor(y_test))
        
        
        dataset = torch.utils.data.TensorDataset(x_test_tensor, y_test_tensor)
        
        test_loader = DataLoader(dataset=dataset,
                              batch_size=len(x_test),
                              num_workers=0,
                              drop_last=True)
        #
        for i_batch, batch in enumerate(test_loader):
            inputs, outputs = batch
            
            x_test, y_test = create_xy_sequences(inputs,
                                                  outputs,
                                                  input_len,
                                                  output_len,
                                                  multiple_targets=True)
            pred = model.testing_step(x_test,i_batch)
            mae, rmse, mape, r2 = regression_report(y_test.detach(), pred.detach(), verbose = False)
            avg_mae = np.mean(mae)
            avg_rmse = np.mean(rmse)
            avg_mape = np.mean(mape)
            avg_r2 = np.mean(r2)
        
            print(f'mae: {avg_mae}, rmse: {avg_rmse}, mape: {avg_mape}, r2: {avg_r2}\n')
        
        test_loss = pd.DataFrame(
        {'mae': mae,
         'rmse':rmse,
         'mape':mape,
         'r2': r2
        })
        test_loss.to_csv(f'./outputs/losses/test_loss/scinet_loss_{company}.csv')

if __name__ == '__main__':
    train()
    test()
