import pandas as pd
import time
from sklearn.model_selection import train_test_split


import os
import argparse
import torch
import torch.optim as optim
import torch.nn as nn
from transformers import BertTokenizer, AutoModelForSequenceClassification

from header.model.utils import fix_seed, load_data
from header.model import TweetRunner
from header.model.BERT import SentimentBERT
from header.preprocessing import TweetDataset


def parse() -> argparse.Namespace:
    parser = argparse.ArgumentParser()
    parser.add_argument('--load', type = str, default = None)
    parser.add_argument('--epochs', type = int, default = 3)
    parser.add_argument('--batch_size', type = int, default = 64)
    parser.add_argument('--lr', type = float, default = 1e-4)
    parser.add_argument('--trainable', action='store_true', default = True)
    args = parser.parse_args()

    print('=' * 70)
    for key, value in vars(args).items():
        print(f'{key}: {value}')
    print('=' * 70)

    return args


def main() -> None:
    args = parse()
    
    # -------- Prepare dataset --------
    df = pd.read_csv('./data/tweet/tweet_pretrained_dataset.csv')

    train, test = train_test_split(df, test_size = 0.25, random_state = 0)
    train.reset_index(drop = True, inplace = True)
    test.reset_index(drop = True, inplace = True)
    
    # prepare tokenizer
    PRE_TRAINED_MODEL_NAME = 'bert-base-cased'
    tokenizer = BertTokenizer.from_pretrained(PRE_TRAINED_MODEL_NAME)
    
    # some hyperparameter for tokenizer
    seq_len = 160
    
    trainset = TweetDataset(
        train['text'], train['target'], tokenizer, seq_len
    )
    
    testset = TweetDataset(
        test['text'], test['target'], tokenizer, seq_len
    )
    
    train_loader = load_data(trainset, batch_size = args.batch_size, shuffle = True)
    test_loader = load_data(testset, batch_size = args.batch_size, shuffle = False)
    
    # -------- Prepare model --------
    # model = SentimentBERT(n_classes = 3, dropout = 0.3)
    model = AutoModelForSequenceClassification.from_pretrained("bert-base-cased", num_labels = 3)

    if args.load:
        model.load_state_dict(torch.load(args.load))

    optimizer = optim.Adam(model.parameters(), lr = args.lr)

    criterion = nn.CrossEntropyLoss()
    scheduler = optim.lr_scheduler.CosineAnnealingLR(optimizer, T_max = 100, eta_min = 1e-7)

    runner = TweetRunner(model, optimizer, criterion)

    

    if args.trainable:
        print(f'Start to train...\n')
        start = time.time()
        runner.train(
            args.epochs, train_loader, 
            valid_loader = test_loader, scheduler = scheduler
        )
        end = time.time()
        print(f'\nEnd in {end - start:.4f}s...\n')
        
        os.makedirs('ckpt', exist_ok=True)
        for key, value in runner.weights.items():
            torch.save(value.state_dict(), os.path.join('ckpt/BERT/', f'{key}.pt'))

    runner.test(test_loader)


if __name__ == '__main__':
    fix_seed(seed=0)

    main()


