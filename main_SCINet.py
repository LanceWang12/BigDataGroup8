import pandas as pd
import time


import os
import argparse
import glob
import torch
import torch.optim as optim
import torch.nn as nn

from header.preprocessing.dataset import StockDataset
from header.model import StockRunner
from header.model.SCINet import SCINet
from header.model.utils import fix_seed, load_data, EarlyStopping


def parse() -> argparse.Namespace:
    parser = argparse.ArgumentParser()
    parser.add_argument('--load', type = str, default = None)
    parser.add_argument('--epochs', type = int, default = 200)
    parser.add_argument('--batch_size', type = int, default = 128)
    parser.add_argument('--lr', type = float, default = 1e-3)
    parser.add_argument('--trainable', action='store_true', default = True)
    parser.add_argument('--device', type = str, default = 'cuda:0')
    # parser.add_argument('--nclass', type = int, default = 3)
    parser.add_argument('--patience', type = int, default = 3)
    parser.add_argument('--delta', type = float, default = 1e-2)
    #parser.add_argument('--freeze', type = bool, default = True)
    
    # -------- parameters for dataset --------
    parser.add_argument('--gap', type = int, default = 10)

    # -------- hyperparameter for Scinet --------
    parser.add_argument('--input_len', type = int, default = 64)
    parser.add_argument('--output_len', type = int, default = 10)
    parser.add_argument('--input_dim', type = int, default = 26)
    parser.add_argument('--hid_size', type = int, default = 1)
    parser.add_argument('--num_stacks', type = int, default = 1)
    parser.add_argument('--num_levels', type = int, default = 3)
    parser.add_argument('--groups', type = int, default = 1)
    parser.add_argument('--kernel', type = int , default = 5)
    parser.add_argument('--dropout', type = float, default = 0.3)

    args = parser.parse_args()

    print('=' * 70)
    for key, value in vars(args).items():
        print(f'{key}: {value}')
    print('=' * 70)

    return args


def main() -> None:
    args = parse()
    
    # -------- Prepare dataset --------
    # train
    # company_path_lst = glob.glob('./data/summary/*.csv')
    # company_path_lst = glob.glob('./data/fe/csv/*.csv')
    company_path_lst = [
        './data/summary/JNJ.csv',
         './data/summary/AXP.csv',
         './data/summary/PFE.csv',
         './data/summary/WBA.csv',
         './data/summary/MMM.csv',
         './data/summary/PG.csv',
         './data/summary/IBM.csv',
         './data/summary/CSCO.csv',
         './data/summary/AAPL.csv',
         './data/summary/BA.csv',
         './data/summary/CAT.csv',
         #'./data/summary/TRV.csv',
         './data/summary/JPM.csv',
         './data/summary/MCD.csv',
         './data/summary/XOM.csv',
         './data/summary/HD.csv',
         #'./data/summary/VZ.csv',
         './data/summary/KO.csv',
         './data/summary/MRK.csv',
         #'./data/summary/V.csv',
         './data/summary/MSFT.csv'
    ]
    trainset = StockDataset(
        company_csv_path_lst = company_path_lst,
        input_len = args.input_len,
        gap = args.gap,
        pred_len = args.output_len,
        train = True
    )
    train_loader = load_data(trainset, batch_size = args.batch_size, shuffle = False, droplast = True)

    # test

    
    # -------- Prepare model --------
    model = SCINet(
        output_len = args.output_len,
        input_len = args.input_len,
        input_dim = args.input_dim,
        hid_size = args.hid_size,
        num_stacks = args.num_stacks,
        num_levels = args.num_levels,
        groups = args.groups,
        kernel = args.kernel,
        dropout = args.dropout
    )



    if args.load:
        model.load_state_dict(torch.load(args.load))

    optimizer = optim.Adam(model.parameters(), lr = args.lr)
    criterion = nn.HuberLoss(delta = 1)
    scheduler = optim.lr_scheduler.CosineAnnealingLR(optimizer, T_max = 100, eta_min = 1e-7)
    
    
    model_ckpt = [
        EarlyStopping(
            patience = args.patience,
            verbose = True,
            delta = args.patience,
            path = './ckpt/SCINet/best_ckpt.pt'
        )
    ]
    

    runner = StockRunner(model, optimizer, criterion, device = args.device)


    if args.trainable:
        print(f'Start to train...\n')
        start = time.time()
        runner.train(
            epochs = args.epochs,
            train_loader = train_loader,
            scheduler = scheduler
        )
        end = time.time()
        print(f'End in {end - start:.4f}s...\n')
        
        os.makedirs('ckpt/SCINet', exist_ok=True)
        for key, value in runner.weights.items():
            torch.save(value.state_dict(), os.path.join('ckpt/SCINet/', f'last_ckpt.pt'))
    
    # model.load_state_dict(torch.load('ckpt/SCINet/best_ckpt.pt'))
    # runner.test(test_loader)


if __name__ == '__main__':
    fix_seed(seed=0)

    main()


