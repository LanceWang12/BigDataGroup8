import glob
import random
import pandas as pd
from typing import Any, Callable, Optional, Sequence, Dict, Tuple, Union, List

import torch
from torch.utils.data import Dataset, DataLoader

from transformers import RobertaTokenizerFast

class TweetDataset(Dataset):
    def __init__(
        self,
        reviews: Sequence,
        targets: Sequence = None,
        tokenizer = None,
        max_len: int = 160,
        inference: bool = False
    ):
        self.reviews = reviews
        self.targets = targets
        
        if tokenizer is None:
            PRE_TRAINED_MODEL_NAME = 'roberta-base'
            self.tokenizer = RobertaTokenizerFast.from_pretrained(PRE_TRAINED_MODEL_NAME)
        else:
            self.tokenizer = tokenizer
            
        self.max_len = max_len
        
        self.inference = inference
        
    def __len__(self):
        return len(self.reviews)
        
    def __getitem__(self, idx):
        if self.inference:
            review = str(self.reviews[idx])
        else:
            review = str(self.reviews[idx])
            target = self.targets[idx]
        
        encoding = self.tokenizer.encode_plus(
                review,
                add_special_tokens=True,
                max_length=self.max_len,
                return_token_type_ids=False,
                padding = 'max_length',
                return_attention_mask=True,
                return_tensors='pt',
                truncation = True
        )
        
        if self.inference:
            return {
                'review_text': review,
                'input_ids': encoding['input_ids'].flatten(),
                'attention_mask': encoding['attention_mask'].flatten()
            }
        else:
            return {
                'review_text': review,
                'input_ids': encoding['input_ids'].flatten(),
                'attention_mask': encoding['attention_mask'].flatten(),
                'targets': torch.tensor(target, dtype=torch.long)
            }
    
class StockDataset(Dataset):
    def __init__(
        self,
        
        # ------- parameters for prediction --------
        input_len: int = 64,
        gap: int = 10,
        pred_len: int = 10,
        pred_target: str = 'Close',
        date_col: str = 'Date',
        
        # -------- parameters for training --------
        train: bool = True,
        company_csv_path_lst: Optional = None,
        
        # -------- parameters for testing --------
        test_start_date: str = '2020-01-01',
        date_format: str = "%Y-%m-%d",
        company_path: Optional = None
    ):
        assert gap > 0, "gap must > 0"
        assert pred_len > 0, "pred_len must > 0"
        assert input_len > 0, "input_len must > 0"
        
        self.gap = gap
        self.input_len = input_len
        self.pred_len = pred_len
        self.pred_target = pred_target
        self.date_col = date_col
        
        self.train = train
        self.date_format = date_format
        self.test_start_date = pd.to_datetime(test_start_date, format = date_format)
        
        # block_len is the length of data for model prediction in one iteration
        self.block_len = input_len + gap + pred_len - 1
        
        if train:
            dataset_size = 0
            for path in company_csv_path_lst:
                tmp = pd.read_csv(path)
                tmp[date_col] = pd.to_datetime(tmp[date_col], format = date_format)
                tmp = tmp[tmp[date_col] < self.test_start_date]
                dataset_size += (tmp.shape[0] - self.block_len)

                del tmp

            self.size = dataset_size + 1
            self.company_lst = company_csv_path_lst
        else:
            self.test = pd.read_csv(company_path)
            self.test[date_col] = pd.to_datetime(self.test[date_col], format = date_format)
            self.test = self.test[self.test[date_col] > self.test_start_date].drop(columns = [date_col])
            self.size = self.test.shape[0] - self.block_len + 1
        
    def __len__(self):
        return self.size
    
    def __getitem__(self, index):
        assert index < self.size, f"[{index}] must be smaller than the size of dataset!"
        
        if self.train:
            company = random.choice(self.company_lst)
            df = pd.read_csv(company)
            df[self.date_col] = pd.to_datetime(df[self.date_col], format = self.date_format)
            df = df[df[self.date_col] < self.test_start_date].drop(columns = [self.date_col])
            idx = random.randint(0, df.shape[0] - self.block_len)

            x = torch.as_tensor(df.iloc[idx: idx + self.input_len].to_numpy(), dtype = torch.float)
            pred_start = idx + self.input_len + self.gap - 1
            y = torch.as_tensor(df[self.pred_target][pred_start: pred_start + self.pred_len].to_numpy(), dtype = torch.float)
        else:
            x = torch.as_tensor(self.test.iloc[index: index + self.input_len].to_numpy(), dtype = torch.float)
            pred_start = index + self.input_len + self.gap - 1
            y = torch.as_tensor(self.test[self.pred_target][pred_start: pred_start + self.pred_len].to_numpy(), dtype = torch.float)
        
        return x, y