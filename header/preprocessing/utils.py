import os
import re
import glob
import pandas as pd

import torch
import torch.nn as nn
import torch.cuda as cuda

from .dataset import TweetDataset
from ..model.utils import load_data
from ..model.BERT import SentimentRoberta

@torch.no_grad()
def sentiment_preprocessing(
    tweet_lst: list,
    Roberta_path: str = None,
    target_col: str = 'Text',
    date_col: str = 'Datetime',
    sentiment_col: str = 'sentiment',
    date_format: str = '%Y-%m-%d',
    batch_size: int = 64,
    device: str = 'cuda',
    save_csv: bool = False,
    save_csv_dir: str = './data/tweet/sentiment_csv' # ex. 'data/csv', please don't add '/' at the last place
) -> None:
    # -------- set device --------
    device = device if cuda.is_available() else 'cpu'
    
    # -------- read model -------
    model = SentimentRoberta(n_class = 3)
    if Roberta_path:
        model.load_state_dict(torch.load(Roberta_path))
    model = model.to(device)
    
    # -------- start to inference --------
    for path in tweet_lst:
        try:
            df = pd.read_csv(path)
        except:
            df = pd.read_csv(path, lineterminator='\n')
        
        # visit every date
        # df[date_col] = pd.to_datetime(df[date_col], format = date_format)
        
        InferSet = TweetDataset(
            reviews = df[target_col],
            max_len = 160,
            inference = True
        )
        
        InferLoader = load_data(
            InferSet,
            batch_size = batch_size,
            shuffle = False
        )
        
        result = []
        for batch in InferLoader:
            x = batch['input_ids'].to(device)
            mask = batch['attention_mask'].to(device)
            out = model(x, mask)
            out = torch.argmax(out, dim = -1).to('cpu').numpy()
            result += list(out)
            
        df[sentiment_col] = result
        
        if save_csv:
            filename = path.split('/')[-1]

            assert save_csv_dir[-1] != '/', "ex. 'data/csv', please don't add '/' at the last place"
            os.makedirs(save_csv_dir, exist_ok = True)

            df.to_csv(f'{save_csv_dir}/{filename}', index = False)

def get_targetCompany(target, dir_lst):
    target_csv = []
    
    for dir_path in dir_lst:
        csv_lst = glob.glob(dir_path)

        idx = -1

        for i, item in enumerate(csv_lst):
            try:
                m = re.search(target, item).group()
            except:
                continue
            else:
                idx = i
                break

        if idx != -1:
            target_csv.append(csv_lst[idx])
            
    return target_csv

def combine_csv(csv_path, store_path, sort_by = 'Date'):
    # Combine *.csv for forbes2000
    csv_lst = glob.glob(csv_path)
    total_df = pd.DataFrame()

    for csv_file in csv_lst:
        df = pd.read_csv(csv_file)
        total_df = pd.concat([total_df, df])

    total_df.sort_values(by = [sort_by], ignore_index = True, inplace = True)
    
    total_df.to_csv(store_path, index = False)
    
def collect_company_tweet(company,company_tweet):
    sym = company[company.columns[0]]
    for i,company_name in enumerate(sym):
        company_ids = company_tweet[company_tweet.ticker_symbol.str.contains(company_name)].tweet_id.tolist()
        company_tweets = tweet[tweet['tweet_id'].isin(company_ids)].reset_index()
        if company_name == 'GOOGL':
            i = 1
        company_tweets['class'] = i
        yield company_tweets