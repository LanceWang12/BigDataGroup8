import datetime
import pandas as pd

def timestamp2datetime(timestamp, Type = 'str'):
    if Type == 'str':
        return datetime.datetime.utcfromtimestamp(timestamp).strftime("%Y/%m/%d %H:%M:%S")
    else:
        return datetime.datetime.utcfromtimestamp(timestamp)
    
def str2datetime(string, pattern = "%d-%m-%Y"):
    return datetime.datetime.strptime(string, pattern)
    
"""
average for x days
""" 
def calculate_average(df, target = 'Close', ma_days = [7, 10, 14, 21, 50, 100]):
    for ma in ma_days:
        column_name = f"MA for {ma} days"
        df[column_name] = pd.DataFrame.rolling(df[target], ma).mean()
        
"""
maximum for x days
""" 
def calculate_maximum(df, target, maxi_days = [7, 30, 365, 730]):
    for ma in maxi_days:
        column_name = f"Maximum for {ma} days"
        df[column_name] = pd.DataFrame.rolling(df[target],ma).max()

"""
minimum for x days
""" 
def calculate_minimum(df, target, maxi_days = [7, 30, 365, 730]):
    for ma in maxi_days:
        column_name = f"Minimum for {ma} days"
        df[column_name] = pd.DataFrame.rolling(df[target],ma).min()
        
