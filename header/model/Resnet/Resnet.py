import torch
import torch.nn as nn
from torch.optim.lr_scheduler import ReduceLROnPlateau

import pytorch_lightning as pl

from .block import BasicBlock, Bottleneck, conv1x1, conv3x3

class ResNet(nn.Module):

    def __init__(
        self, input_depth, block, layers, num_classes=1000, zero_init_residual=False,
        groups=1, width_per_group=64, replace_stride_with_dilation=None,
        norm_layer=None
    ):
        super(ResNet, self).__init__()
        if norm_layer is None:
            norm_layer = nn.BatchNorm1d
        self._norm_layer = norm_layer

        self.inplanes = 64
        self.dilation = 1
        if replace_stride_with_dilation is None:
            # each element in the tuple indicates if we should replace
            # the 2x2 stride with a dilated convolution instead
            replace_stride_with_dilation = [False, False, False]
        if len(replace_stride_with_dilation) != 3:
            raise ValueError("replace_stride_with_dilation should be None "
                             "or a 3-element tuple, got {}".format(replace_stride_with_dilation))
        self.groups = groups
        self.base_width = width_per_group
        self.conv1 = nn.Conv1d(input_depth, self.inplanes, kernel_size=7, stride=2, padding=3,
                               bias=False)
        self.bn1 = norm_layer(self.inplanes)
        self.relu = nn.ReLU(inplace=True)
        self.maxpool = nn.MaxPool1d(kernel_size=3, stride=2, padding=1)
        self.layer1 = self._make_layer(block, 64, layers[0])
        self.layer2 = self._make_layer(block, 128, layers[1], stride=2,
                                       dilate=replace_stride_with_dilation[0])
        self.layer3 = self._make_layer(block, 256, layers[2], stride=2,
                                       dilate=replace_stride_with_dilation[1])
        self.layer4 = self._make_layer(block, 512, layers[3], stride=2,
                                       dilate=replace_stride_with_dilation[2])
        self.avgpool = nn.AdaptiveAvgPool1d(1)
        self.fc = nn.Linear(512 * block.expansion, num_classes)

        for m in self.modules():
            if isinstance(m, nn.Conv1d):
                nn.init.kaiming_normal_(m.weight, mode='fan_out', nonlinearity='relu')
            elif isinstance(m, (nn.BatchNorm1d, nn.GroupNorm)):
                nn.init.constant_(m.weight, 1)
                nn.init.constant_(m.bias, 0)

        # Zero-initialize the last BN in each residual branch,
        # so that the residual branch starts with zeros, and each residual block behaves like an identity.
        # This improves the model by 0.2~0.3% according to https://arxiv.org/abs/1706.02677
        if zero_init_residual:
            for m in self.modules():
                if isinstance(m, Bottleneck):
                    nn.init.constant_(m.bn3.weight, 0)
                elif isinstance(m, BasicBlock):
                    nn.init.constant_(m.bn2.weight, 0)

    def _make_layer(self, block, planes, blocks, stride=1, dilate=False):
        norm_layer = self._norm_layer
        downsample = None
        previous_dilation = self.dilation
        if dilate:
            self.dilation *= stride
            stride = 1
        if stride != 1 or self.inplanes != planes * block.expansion:
            downsample = nn.Sequential(
                conv1x1(self.inplanes, planes * block.expansion, stride),
                norm_layer(planes * block.expansion),
            )

        layers = []
        layers.append(block(self.inplanes, planes, stride, downsample, self.groups,
                            self.base_width, previous_dilation, norm_layer))
        self.inplanes = planes * block.expansion
        for _ in range(1, blocks):
            layers.append(block(self.inplanes, planes, groups=self.groups,
                                base_width=self.base_width, dilation=self.dilation,
                                norm_layer=norm_layer))

        return nn.Sequential(*layers)

    def _forward_impl(self, x):
        # See note [TorchScript super()]
        x = self.conv1(x)
        x = self.bn1(x)
        x = self.relu(x)
        x = self.maxpool(x)

        x = self.layer1(x)
        x = self.layer2(x)
        x = self.layer3(x)
        x = self.layer4(x)

        x = self.avgpool(x)
        x = torch.flatten(x, 1)
        x = self.fc(x)

        return x

    def forward(self, x):
        return self._forward_impl(x)

class PL_ResNet(pl.LightningModule):
    def __init__(
        self, input_depth, block, layers, num_classes=1000, zero_init_residual=False,
        groups=1, width_per_group=64, replace_stride_with_dilation=None,
        norm_layer=None, lr = 1e-2
    ):
        super().__init__()
        
        self.lr = lr
        
        self.resnet = ResNet(
            input_depth = input_depth, block = block, layers = layers, 
            num_classes = num_classes, zero_init_residual = zero_init_residual,
            groups = groups, width_per_group = width_per_group, 
            replace_stride_with_dilation = replace_stride_with_dilation, 
            norm_layer = norm_layer
        )
        
        self.set_criterion()
        
        self.metric = {'train_loss': [], 'val_loss': [], 'test_result': []}
        
    def forward(self, x):
        x = self.resnet(x)
        return x
    
    def training_step(self, batch, batch_idx):
        batch_x, _, batch_y = batch
        pred = self(batch_x)
        
        loss = self.criterion(pred, batch_y)
        # self.log("train_loss", loss, prog_bar=True)
        # self.log_dict({"loss": loss}, prog_bar=True)
        
        return loss
    
    def validation_step(self, batch, batch_idx):
        batch_x, _, batch_y = batch
        pred = self(batch_x)
        
        loss = self.criterion(pred, batch_y)
        # self.log("train_loss", loss, prog_bar=True)
        # self.log_dict({"val_loss": loss}, prog_bar = False)
        
        return loss
    
    def testing_step(self, batch, batch_idx):
        batch_x, _, batch_y = batch
        pred = self(batch_x)
        
        loss = self.criterion(pred, batch_y)
        
        # self.log("train_loss", loss, prog_bar=True)
        
        return {'test_loss': loss, 'pred': pred, 'y_true': batch_y}
    
    def training_epoch_end(self, training_step_outputs):
        mean = 0
        for i, out in enumerate(training_step_outputs):
            #print(f'hi, {type(out)}')
            #print(out['loss'].item())
            mean += out['loss'].item()
        
        avg_loss = mean / i
        self.metric['train_loss'].append(avg_loss)
        
    def validation_epoch_end(self, validation_step_outputs):
        mean = 0
        for i, out in enumerate(validation_step_outputs):
            mean += out.item()
            #pass
        
        #avg_loss = torch.mean(training_step_outputs)
        avg_loss = mean / i
        self.metric['val_loss'].append(avg_loss)
        
        self.log_dict({"val_loss": avg_loss}, prog_bar = True)
    
    def testing_epoch_end(self, testing_step_outputs):
        for i, out in enumerate(testing_step_outputs):
            mean += out['test_loss'].item()
            
            if i == 0:
                pred = out['pred']
                y_true = out['y_true']
            else:
                pred = torch.cat((pred, out['pred']), 0)
                y_true = torch.cat((y_true, out['y_true']), 0)
                
        
    
    def configure_optimizers(self):
        optimizer = torch.optim.Adam(self.parameters(), lr = self.lr)
        
        scheduler = ReduceLROnPlateau(
            optimizer, mode = 'min', factor = 0.4,
            patience = 5, threshold = 1e-3, verbose = False
        )
        
        return {
            "optimizer": optimizer,
            "lr_scheduler": {
                "scheduler": scheduler,
                "monitor": "val_loss",
                "frequency": 1
            }
        }
        
    def set_criterion(self):
        # self.criterion = nn.MSELoss()
        self.criterion = nn.HuberLoss(reduction = 'mean', delta = 1.0)


def _resnet(arch, input_depth, num_classes, block, layers, pretrained, progress, using_mode = 'PL', **kwargs):
    if using_mode == 'PL':
        model = PL_ResNet(input_depth, block, layers, num_classes, **kwargs)
    else:
        model = ResNet(input_depth, block, layers, num_classes, **kwargs)
    
    """
    if pretrained:
        state_dict = load_state_dict_from_url(model_urls[arch],
                                              progress=progress)
        model.load_state_dict(state_dict)
    """
    return model

def resnet18(input_depth, num_classes,pretrained=False, progress=True, **kwargs):
    r"""ResNet-18 model from
    `"Deep Residual Learning for Image Recognition" <https://arxiv.org/pdf/1512.03385.pdf>`_

    Args:
        pretrained (bool): If True, returns a model pre-trained on ImageNet
        progress (bool): If True, displays a progress bar of the download to stderr
    """
    return _resnet('resnet18', input_depth, num_classes, BasicBlock, [2, 2, 2, 2], pretrained, progress,
                   **kwargs)

def resnet34(input_depth, num_classes, pretrained=False, progress=True, **kwargs):
    r"""ResNet-34 model from
    `"Deep Residual Learning for Image Recognition" <https://arxiv.org/pdf/1512.03385.pdf>`_

    Args:
        pretrained (bool): If True, returns a model pre-trained on ImageNet
        progress (bool): If True, displays a progress bar of the download to stderr
    """
    return _resnet('resnet34', input_depth, num_classes, BasicBlock, [3, 4, 6, 3], pretrained, progress, **kwargs)



def resnet50(input_depth, num_classes, pretrained=False, progress=True, **kwargs):
    r"""ResNet-50 model from
    `"Deep Residual Learning for Image Recognition" <https://arxiv.org/pdf/1512.03385.pdf>`_

    Args:
        pretrained (bool): If True, returns a model pre-trained on ImageNet
        progress (bool): If True, displays a progress bar of the download to stderr
    """
    return _resnet('resnet50', input_depth, num_classes, Bottleneck, [3, 4, 6, 3], pretrained, progress, **kwargs)



def resnet101(input_depth, num_classes, pretrained=False, progress=True, **kwargs):
    r"""ResNet-101 model from
    `"Deep Residual Learning for Image Recognition" <https://arxiv.org/pdf/1512.03385.pdf>`_

    Args:
        pretrained (bool): If True, returns a model pre-trained on ImageNet
        progress (bool): If True, displays a progress bar of the download to stderr
    """
    return _resnet('resnet101', input_depth, num_classes, Bottleneck, [3, 4, 23, 3], pretrained, progress, **kwargs)



def resnet152(input_depth, num_classes, pretrained=False, progress=True, **kwargs):
    r"""ResNet-152 model from
    `"Deep Residual Learning for Image Recognition" <https://arxiv.org/pdf/1512.03385.pdf>`_

    Args:
        pretrained (bool): If True, returns a model pre-trained on ImageNet
        progress (bool): If True, displays a progress bar of the download to stderr
    """
    return _resnet('resnet152', input_depth, num_classes, Bottleneck, [3, 8, 36, 3], pretrained, progress, **kwargs)