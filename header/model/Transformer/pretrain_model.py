import math

import torch
import torch.nn as nn
from torch.optim.lr_scheduler import ReduceLROnPlateau, CosineAnnealingLR

import pytorch_lightning as pl

from ..Informer.embed import DataEmbedding

class PretrainedTransformer(nn.Module):
    def __init__(
        self, input_depth, seq_len = 64, d_model = 512, 
        n_head = 8, d_hid = 512, nlayers = 3, 
        dropout = 0.5, freq = 3 ,activation = 'gelu'
    ):
        super().__init__()
        self.src_mask = None
        self.d_model = d_model
        
        self.embedding = DataEmbedding(
            c_in = input_depth, d_model = d_model, embed_type = 'timeF', 
            freq = freq, dropout = dropout
        )
        
        encoder_layer = nn.TransformerEncoderLayer(
            d_model = d_model, nhead = n_head, activation = activation,
            dim_feedforward = d_hid, dropout = dropout
        )
        
        self.transformer_encoder = nn.TransformerEncoder(encoder_layer, nlayers)
        
        self.conv = nn.Conv1d(d_model, input_depth, kernel_size = 1, stride = 1)
        
        self.gelu = nn.GELU()
    
    def forward(self, x, t):
        embed = self.embedding(x, t)
        out = self.transformer_encoder(embed) #, self.src_mask)
        out = self.gelu(out)
        out = self.conv(out.transpose(1, 2))
        out = self.gelu(out.transpose(1, 2))
        
        return out

class PL_PretrainedTransformer(pl.LightningModule):
    def __init__(
            self, input_depth, seq_len = 64, d_model = 512, n_head = 8, 
            d_hid = 512, nlayers = 3, dropout = 0.5, freq = 3, lr = 1e-4,
            activation = 'gelu'
    ):
        super().__init__()
        # Important: This property activates manual optimization.
        self.automatic_optimization = False
        
        self.lr = lr
        
        self.encoder = PretrainedTransformer(
            input_depth = input_depth,seq_len = seq_len, d_model = d_model, 
            n_head = n_head, d_hid = d_hid, nlayers = nlayers, 
            dropout = dropout, freq = freq, activation = activation
        )
        
        self.set_criterion()
        
        self.metric = {'train_loss': [], 'val_loss': [], 'test_result': []}
    
    def forward(self, x, t):
        out = self.encoder(x, t)
        
        return out
    
    def training_step(self, batch, batch_idx):
        batch_x, t, _ = batch
        
        self.optimizer.zero_grad()
        
        pred = self(batch_x, t)
        
#         print(batch_x.shape, pred.shape)
        
        loss = self.criterion(pred, batch_x)
        
        self.manual_backward(loss)
        
        nn.utils.clip_grad_norm_(self.parameters(), 0.5)
        
        self.optimizer.step()
        self.scheduler.step()
        # self.log("train_loss", loss, prog_bar=True)
        self.log_dict({"loss": loss}, prog_bar=True)
        
        return loss
    
    def validation_step(self, batch, batch_idx):
        batch_x, t, _ = batch
        pred = self(batch_x, t)
        
        loss = self.criterion(pred, batch_x)
        # self.log("train_loss", loss, prog_bar=True)
        # self.log_dict({"val_loss": loss}, prog_bar = False)
        
        return loss
    
    def testing_step(self, batch, batch_idx):
        batch_x, t, _ = batch
        pred = self(batch_x)
        
        loss = self.criterion(pred, batch_x)
        
        # self.log("train_loss", loss, prog_bar=True)
        
        return {'test_loss': loss, 'pred': pred, 'y_true': batch_y}
    
    def training_epoch_end(self, training_step_outputs):
        mean = 0
        for i, out in enumerate(training_step_outputs):
            #print(f'hi, {type(out)}')
            #print(out['loss'].item())
            mean += out['loss'].item()
        
        avg_loss = mean / i
        self.metric['train_loss'].append(avg_loss)
        
    def validation_epoch_end(self, validation_step_outputs):
        mean = 0
        for i, out in enumerate(validation_step_outputs):
            mean += out.item()
            #pass
        
        #avg_loss = torch.mean(training_step_outputs)
        avg_loss = mean / i
        self.metric['val_loss'].append(avg_loss)
        
        self.log_dict({"val_loss": avg_loss}, prog_bar = True)
    
    def testing_epoch_end(self, testing_step_outputs):
        for i, out in enumerate(testing_step_outputs):
            mean += out['test_loss'].item()
            
            if i == 0:
                pred = out['pred']
                y_true = out['y_true']
            else:
                pred = torch.cat((pred, out['pred']), 0)
                y_true = torch.cat((y_true, out['y_true']), 0)
                
        
    
    def configure_optimizers(self):
        self.optimizer = torch.optim.Adam(self.parameters(), lr = self.lr)
        
        """
        scheduler = ReduceLROnPlateau(
            optimizer, mode = 'min', factor = 0.4,
            patience = 5, threshold = 1e-3, verbose = False
        )
        """
        self.scheduler = CosineAnnealingLR(
            self.optimizer, T_max = 12,
            eta_min = 1e-6
        )
        """
        return {
            "optimizer": self.optimizer,
            "lr_scheduler": {
                "scheduler": self.scheduler,
                "monitor": "val_loss",
                "frequency": 1
            }
        }
        """
        
    def set_criterion(self):
        # self.criterion = nn.MSELoss()
        self.criterion = nn.HuberLoss(reduction = 'mean', delta = 1.0)