import torch
import torch.nn as nn
import torch.nn.functional as F
from torch.optim.lr_scheduler import ReduceLROnPlateau, CosineAnnealingLR

import pytorch_lightning as pl

from .model import LanceInformer



class PL_Informer(pl.LightningModule):
    def __init__(
        self, input_depth, seq_len, output_depth = 1, factor = 5, 
        d_model = 512, n_heads = 8, e_layers = 3, d_ff = 512, dropout = 0.0, 
        attn = 'prob', embed = 'timeF', freq = 3, activation = 'gelu', 
        output_attention = False, distil = True, mix = True, lr = 1e-3
    ):
        super().__init__()
        
        # Important: This property activates manual optimization.
        self.automatic_optimization = False
        
        self.lr = lr
        self.output_attention = output_attention
        
        self.encoder = LanceInformer(
            input_depth = input_depth, seq_len = seq_len, 
            output_depth = output_depth, factor = factor, 
            d_model = d_model, n_heads = n_heads, 
            e_layers = e_layers, d_ff = d_ff, dropout = dropout, 
            attn = attn, embed = embed, freq = freq, 
            activation = activation, output_attention = output_attention, 
            distil = distil, mix = mix
        )
        
        self.set_criterion()
        
        self.metric = {'train_loss': [], 'val_loss': [], 'test_result': []}
        
    def forward(
            self, x_enc, t_enc, 
            enc_self_mask=None, dec_self_mask=None, dec_enc_mask=None
    ):
        if self.output_attention:
            out, attns = self.encoder(x_enc, t_enc, enc_self_mask=None, dec_self_mask=None, dec_enc_mask=None)
        else:
            out = self.encoder(x_enc, t_enc, enc_self_mask=None, dec_self_mask=None, dec_enc_mask=None)
            
        return out
        
    def training_step(self, batch, batch_idx):
        batch_x, t, batch_y = batch
        
        self.optimizer.zero_grad()
        
        pred = self(batch_x, t)
        
        loss = self.criterion(pred, batch_y)
        
        self.manual_backward(loss)
        
        nn.utils.clip_grad_norm_(self.parameters(), 0.5)
        
        self.optimizer.step()
        self.scheduler.step()
        # self.log("train_loss", loss, prog_bar=True)
        self.log_dict({"loss": loss}, prog_bar=True)
        
        return loss
    
    def validation_step(self, batch, batch_idx):
        batch_x, batch_t, batch_y = batch
        
        if self.output_attention:
            pred, _ = self(batch_x, batch_t)
        else:
            pred = self(batch_x, batch_t)
        
        loss = self.criterion(pred, batch_y)
        # self.log("val_loss", loss, prog_bar=True)
        
        return loss
    
    def testing_step(self, batch, batch_idx):
        batch_x, batch_t, batch_y = batch
        
        if self.output_attention:
            pred, _ = self(batch_x, batch_t)
        else:
            pred = self(batch_x, batch_t)
        return pred
    
    def training_epoch_end(self, training_step_outputs):
        mean = 0
        for i, out in enumerate(training_step_outputs):
            #print(f'hi, {type(out)}')
            #print(out['loss'].item())
            mean += out['loss'].item()
        
        avg_loss = mean / i
        self.metric['train_loss'].append(avg_loss)
        
    def validation_epoch_end(self, validation_step_outputs):
        mean = 0
        for i, out in enumerate(validation_step_outputs):
            mean += out.item()
            #pass
        
        avg_loss = mean / i
        self.metric['val_loss'].append(avg_loss)
        self.log_dict({"val_loss": avg_loss}, prog_bar = True)
    
    def testing_epoch_end(self, testing_step_outputs):
        for i, out in enumerate(testing_step_outputs):
            #print(f'hi, {type(out)}')
            #print(out['loss'].item())
            mean += out['loss'].item()
    
    def configure_optimizers(self):
        self.optimizer = torch.optim.Adam(self.parameters(), lr = self.lr)
        
        """
        scheduler = ReduceLROnPlateau(
            optimizer, mode = 'min', factor = 0.4,
            patience = 5, threshold = 1e-3, verbose = False
        )
        """
        self.scheduler = CosineAnnealingLR(
            self.optimizer, T_max = 15,
            eta_min = 1e-6
        )
        
        return {
            "optimizer": self.optimizer,
            "lr_scheduler": self.scheduler
        }
        
        """
        return {
            "optimizer": self.optimizer,
            "lr_scheduler": {
                "scheduler": self.scheduler,
                "monitor": "val_loss",
                "frequency": 1
            }
        }
        """
        
    def set_criterion(self):
        # self.criterion = nn.MSELoss()
        self.criterion = nn.HuberLoss(reduction='mean', delta=1.0)
        

