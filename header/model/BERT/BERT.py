import transformers
from transformers import BertModel, RobertaForSequenceClassification

import torch
from torch import nn, optim



class SentimentBERT(nn.Module):
    def __init__(self, n_class = 3, dropout = 0.3) -> None:
        super().__init__()
        self.BERT = BertModel.from_pretrained('bert-base-cased')
        self.dropout = nn.Dropout(p = dropout)
        self.linear = nn.Linear(self.BERT.config.hidden_size, n_class)

    def forward(self, x, mask):
        pooled_out = self.BERT(
            input_ids = x,
            attention_mask = mask
        )['pooler_output']
        out = self.dropout(pooled_out)

        out = self.linear(out)

        return out

class SentimentRoberta(nn.Module):
    def __init__(self, n_class = 3, dropout = 0.3) -> None:
        super().__init__()
        
        self.Roberta = RobertaForSequenceClassification.from_pretrained(
            "cardiffnlp/twitter-roberta-base-emotion",
            problem_type = "multi_label_classification"
        )
        
        hid = self.Roberta.config.hidden_size
        self.Roberta.classifier.out_proj = nn.Linear(hid, n_class)
        
    def freeze(self):
        for param in self.Roberta.roberta.parameters():
            param.requires_grad = False
        
    def forward(self, x, mask):
        
        out = self.Roberta(
            input_ids = x,
            attention_mask = mask
        )['logits']
        
        return out