from typing import Any, Callable, Optional, Sequence, Dict, Tuple, Union, List

from sklearn.metrics import mean_absolute_error as MAE
from sklearn.metrics import mean_squared_error as MSE
from sklearn.metrics import mean_absolute_percentage_error as MAPE
from sklearn.metrics import r2_score as R2

import torch
import torch.optim as optim
import torch.cuda as cuda
from torch.utils.data import DataLoader
import torch.nn as nn
from .utils import ProgressBar, EarlyStopping

class _History:
    def __init__(
        self,
        metrics: Sequence[str] = ['loss', 'mae', 'rmse', 'mape', 'r2'],
        additional_keys: Sequence[str] = []
    ) -> None:
        self.metrics = metrics
        self.additional_keys = additional_keys

        self._history = {
            'loss': [],
            'mae': [],
            'rmse': [],
            'mape': [],
            'r2': []
        }

        for key in self.additional_keys:
            self._history[key] = []

    def __str__(self) -> str:
        results = []

        for metric in self.metrics:
            results.append(f'{metric}: {self._history[metric][-1]:.3f}')

        return ', '.join(results)

    def __getitem__(self, idx: int) -> Dict[str, Union[int, float]]:
        results = {}

        for metric in self.metrics:
            results[metric] = self._history[metric][idx]

        return results

    def reset(self) -> None:
        for key in self._history.keys():
            self._history[key].clear()

    def log(self, key: str, value: Any) -> None:
        self._history[key].append(value)

    def summary(self) -> None:
        length = len(self._history['loss'])
        _loss = sum(self._history['loss']) / length
        _mae = sum(self._history['mae']) / length
        _rmse = sum(self._history['rmse']) / length
        _mape = sum(self._history['mape']) / length
        _r2 = sum(self._history['r2']) / length

        self._history['loss'].append(_loss)
        self._history['mae'].append(_mae)
        self._history['rmse'].append(_rmse)
        self._history['mape'].append(_mape)
        self._history['r2'].append(_r2)

        for key in self.additional_keys:
            _value = sum(self._history[key]) / len(self._history[key])
            self._history[key].append(_value)


class _BaseRunner:
    def __init__(self, device = 'cuda') -> None:
        self.device = device if cuda.is_available() else 'cpu'

    @property
    def weights(self) -> None:
        raise NotImplementedError('weights not implemented')


class StockRunner(_BaseRunner):
    def __init__(
        self,
        net,
        optimizer: optim.Optimizer,
        criterion: Callable,
        model_ckpt: Optional[Callable] = None,
        device: str = 'cuda'
    ) -> None:
        super().__init__(device)

        self.history = _History(metrics=['loss', 'mae', 'rmse', 'mape', 'r2'])

        self.net = net
        self.optimizer = optimizer
        self.criterion = criterion
        self.model_ckpt = model_ckpt

        self.net = self.net.to(self.device)

    def _step(self, batch) -> torch.Tensor:
        x, y = batch
        x = x.to(self.device)
        y = y.to(self.device)
        output = self.net(x)
        
#         print(y.shape, output.shape)
        
        # y_hat = torch.argmax(output, dim=-1)
        running_loss = self.criterion(output, y)
        
        nn.utils.clip_grad_value_(self.net.parameters(), clip_value = 1.0)
        
        # log to history
        self.history.log('loss', running_loss)
        
        y_pred = output.detach().to('cpu').numpy()
        y_true = y.detach().to('cpu').numpy()
        mae = MAE(y_true, y_pred)
        rmse = MSE(y_true, y_pred) ** 0.5
        mape = MAPE(y_true, y_pred)
        r2 = R2(y_true, y_pred)
        
        self.history.log('mae', mae)
        self.history.log('rmse', rmse)
        self.history.log('mape', mape)
        self.history.log('r2', r2)

        return running_loss

    def train(self, epochs: int, train_loader: DataLoader, valid_loader: Optional[DataLoader] = None, scheduler: Any = None) -> None:
        epoch_length = len(str(epochs))

        for epoch in range(epochs):
            self.net.train()
            for i, batch in enumerate(train_loader):
                running_loss = self._step(batch)

                self.optimizer.zero_grad()
                running_loss.backward()
                self.optimizer.step()

                prefix = f'Epochs: {(epoch + 1):>{epoch_length}} / {epochs}'
                postfix = str(self.history)
                ProgressBar.show(prefix, postfix, i, len(train_loader))

            self.history.summary()

            prefix = f'Epochs: {(epoch + 1):>{epoch_length}} / {epochs}'
            postfix = str(self.history)
            ProgressBar.show(prefix, postfix, len(train_loader), len(train_loader), newline=True)

            self.history.reset()

            if valid_loader:
                flag = self.val(valid_loader)
                if not flag:
                    break

            if scheduler:
                scheduler.step()

    @torch.no_grad()
    def val(self, test_loader: DataLoader) -> None:
        self.net.eval()
        flag = True
        for i, batch in enumerate(test_loader):
            running_loss = self._step(batch)
            prefix = 'Val'
            postfix = str(self.history)
            ProgressBar.show(prefix, postfix, i, len(test_loader))

        self.history.summary()

        prefix = 'Val'
        postfix = str(self.history)
        ProgressBar.show(prefix, postfix, len(test_loader), len(test_loader), newline=True)
        
        if self.model_ckpt is not None:
            flag = self.model_ckpt(self.history[-1]['loss'], self.net)

        self.history.reset()
        return flag
    
    @torch.no_grad()
    def test(self, test_loader: DataLoader) -> None:
        self.net.eval()

        for i, batch in enumerate(test_loader):
            running_loss = self._step(batch)
            prefix = 'Test'
            postfix = str(self.history)
            ProgressBar.show(prefix, postfix, i, len(test_loader))

        self.history.summary()

        prefix = 'Test'
        postfix = str(self.history)
        ProgressBar.show(prefix, postfix, len(test_loader), len(test_loader), newline=True)

        self.history.reset()

    @property
    @torch.no_grad()
    def weights(self):
        return {'net': self.net}
