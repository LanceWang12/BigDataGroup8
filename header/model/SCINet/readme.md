# SCINet

[toc]

## Hyperparameter explaination

| Parameter Name |         Description         | Parameter in paper | Default |
|:--------------:|:---------------------------:|:------------------:|:-------:|
|   output_len   |           Horizon           |      Horizon       |    x    |
|   input_len    |      input data length      |  Look-back window  |    x    |
|    hid-size    |      hidden expansion       |         h          |    1    |
|   num_levels   |     SCINet block levels     |         L          |    3    |
|   num_stacks   | The number of SCINet blocks |         K          |    1    |
|     kernel     |         kernel size         |                    |    5    |
|    dropout     |        dropout rate         |                    |   0.5   |

## File explaination

1. SCINet.py: original version from [SCINet repository](https://github.com/cure-lab/SCINet)
2. PL_version.py: wrapped by PyTorch Lightning

## How to use?
```python=
from header.model.SCINet import PL_SCINet, SCINet
from header.model.SCINet import * # will import both PL_SCINet and SCINet

# --- Your Code ---
```
