import torch
import torch.nn as nn
from torch.optim.lr_scheduler import ReduceLROnPlateau

import pytorch_lightning as pl

from .SCINet import SCINet

class PL_SCINet(pl.LightningModule):
    def __init__(
        self, input_len, output_len, input_dim = 9, hid_size = 1, num_stacks = 1,
        num_levels = 3, concat_len = 0, groups = 1, kernel = 5, dropout = 0.5,
        single_step_output_One = 0, input_len_seg = 0, positionalE = False, 
        modified = True, RIN=False, lr = 1e-4
    ):
        super().__init__()
        
        self.scinet = SCINet(
            input_len = input_len, output_len = output_len, input_dim = input_dim, 
            hid_size = hid_size, num_stacks = num_stacks, num_levels = num_levels,
            concat_len = concat_len, groups = groups, kernel = kernel, dropout = dropout,
            single_step_output_One = single_step_output_One, input_len_seg = input_len_seg, 
            positionalE = positionalE, modified = modified, RIN = RIN
        )
        
        self.lr = lr
        
        self.set_criterion()
        
        self.metric = {'train_loss': [], 'val_loss': [], 'test_result': []}
    
    def forward(self, x):
        out = self.scinet(x)
        return out
    
    def training_step(self, batch, batch_idx):
        batch_x, _, batch_y = batch
        pred = self(batch_x)
        
        loss = self.criterion(pred, batch_y)
        # self.log("train_loss", loss, prog_bar=True)
        # self.log_dict({"loss": loss}, prog_bar=True)
        
        return loss
    
    def validation_step(self, batch, batch_idx):
        batch_x, _, batch_y = batch
        pred = self(batch_x)
        
        loss = self.criterion(pred, batch_y)
        # self.log("train_loss", loss, prog_bar=True)
        # self.log_dict({"val_loss": loss}, prog_bar = False)
        
        return loss
    
    def testing_step(self, batch, batch_idx):
        batch_x, _, batch_y = batch
        pred = self(batch_x)
        
        loss = self.criterion(pred, batch_y)
        
        # self.log("train_loss", loss, prog_bar=True)
        
        return {'test_loss': loss, 'pred': pred, 'y_true': batch_y}
    
    def training_epoch_end(self, training_step_outputs):
        mean = 0
        for i, out in enumerate(training_step_outputs):
            #print(f'hi, {type(out)}')
            #print(out['loss'].item())
            mean += out['loss'].item()
        
        avg_loss = mean / i
        self.metric['train_loss'].append(avg_loss)
        
    def validation_epoch_end(self, validation_step_outputs):
        mean = 0
        for i, out in enumerate(validation_step_outputs):
            mean += out.item()
            #pass
        
        #avg_loss = torch.mean(training_step_outputs)
        avg_loss = mean / i
        self.metric['val_loss'].append(avg_loss)
        
        self.log_dict({"val_loss": avg_loss}, prog_bar = True)
    
    def testing_epoch_end(self, testing_step_outputs):
        for i, out in enumerate(testing_step_outputs):
            mean += out['test_loss'].item()
            
            if i == 0:
                pred = out['pred']
                y_true = out['y_true']
            else:
                pred = torch.cat((pred, out['pred']), 0)
                y_true = torch.cat((y_true, out['y_true']), 0)
                
        
    
    def configure_optimizers(self):
        optimizer = torch.optim.Adam(self.parameters(), lr = self.lr)
        
        scheduler = ReduceLROnPlateau(
            optimizer, mode = 'min', factor = 0.4,
            patience = 5, threshold = 1e-3, verbose = False
        )
        
        return {
            "optimizer": optimizer,
            "lr_scheduler": {
                "scheduler": scheduler,
                "monitor": "val_loss",
                "frequency": 1
            }
        }
        
    def set_criterion(self):
        # self.criterion = nn.MSELoss()
        self.criterion = nn.HuberLoss(reduction = 'mean', delta = 1.0)