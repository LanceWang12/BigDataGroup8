from datetime import datetime

def getHourFromStr(timestr, pattern = '%Y-%m-%d %H:%M:%S'):
    date = datetime.strptime(timestr, pattern)
    return date.hour