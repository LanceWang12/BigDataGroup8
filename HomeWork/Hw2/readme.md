---
tags: BigData
---


# Hw2

[toc]

## Q1: Find the maximal delays (you should consider both ArrDelay and DepDelay) for each month of 2007.

1. Methods:
    - Get total delay by adding ArrDelay and DepDelay
    - Visit every month and get their total delay
2. Ans:
    | Month | Maximal delays (ArrDelay + DepDelay) |
    |:-----:|:------------------------------------:|
    |   1   |                 2832                 |
    |   2   |                 2699                 |
    |   3   |                 3111                 |
    |   4   |                 2817                 |
    |   5   |                 2845                 |
    |   6   |                 2711                 |
    |   7   |                 2755                 |
    |   8   |                 2921                 |
    |   9   |                 3354                 |
    |  10   |                 5199                 |
    |  11   |                 2283                 |
    |  12   |                 3898                 |




## Q2: How many flights were delayed caused by security between 2000 ~ 2005? Please show the counting for each year.

1. Methods:
    - Visit 2000.csv ~ 2005.csv
    - Determine whether 50% value of security delay is nan in that year
    - Get the sum of SecurityDelay
2. Ans:
    | Year |              Flights               |
    |:----:|:----------------------------------:|
    | 2000 | 50% value of security delay is nan |
    | 2001 | 50% value of security delay is nan |
    | 2002 | 50% value of security delay is nan |
    | 2003 |               80663                |
    | 2004 |               179219               |
    | 2005 |               141045               |




## Q3: List Top 5 airports which occur delays most and least in 2008. (Please show the IATA airport code)

1. Methods:
    - Consider both ArrDelay and DepDelay as total delays
    - Get unique value from "Origin" column to get IATA code
    - Visit every IATA code
    - Get the sum of their total delays
    - Sorting

2. Ans:
    - Top 5 airports which delays most in 2008.

        | IATA code |  delays  |
        |:---------:|:--------:|
        |    DEN    | 4267906  |
        |    EWR    | 4724051  |
        |    DFW    | 6089793  |
        |    ATL    | 9070949  |
        |    ORD    | 11073948 |

    - Top 5 airports which delays least in 2008.

        | IATA code | delays |
        |:---------:|:------:|
        |    PIH    | -17537 |
        |    COD    | -13726 |
        |    SUN    | -11120 |
        |    GTF    | -8943  |
        |    PSC    | -7659  |
