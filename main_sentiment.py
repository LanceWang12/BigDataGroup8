import glob
import os
# import numpy as np
import pandas as pd

from header.preprocessing import sentiment_preprocessing

if __name__ == '__main__':
    result = sentiment_preprocessing(
        tweet_lst = glob.glob('./data/tweet/SP500_tweet_csv/*.csv'),
        Roberta_path = './ckpt/Roberta/last_ckpt.pt',
        target_col = 'Text',
        date_col = 'Datetime',
        sentiment_col = 'sentiment',
        date_format = '%Y-%m-%d',
        batch_size = 128,
        device = 'cuda:2',
        save_csv = True,
        save_csv_dir = './data/tweet/sentiment_csv'
    )
