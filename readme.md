# Big Data Group08

[toc]

## Spec
1. Predict the close value of 14 company after two weeks
2. Test the model from 2020/01/01 to 2022/01/01
3. Train the model from 2014/01/01 to 2019/12/31


## Model Architecture
![](https://i.imgur.com/DblENS4.png)



## Task

1. Data preprocessing
    - csv 檔案串接 (每家公司大概有一千初的csv檔需要串接)
    - twitter 推文文字整理 (有很多推文含了網址等無意義資訊，需要刪除)
2. Model design
    - BERT fine-tuning
    - Model training for numeric data
    - Multi-modal model training
3. Programming on Spark platform
4. Making video clip

## Dataset
1. [Tweets about the Top Companies from 2015 to 2020](https://www.kaggle.com/omermetinn/tweets-about-the-top-companies-from-2015-to-2020)
    - Content:
        This dataset contains over 3 million unique tweets with their information such as tweet id, author of the tweet, post date, the text body of the tweet, and the number of comments, likes, and retweets of tweets matched with the related company.
    - Acknowledgements:
        Tweets are collected from Twitter by a parsing script that is based on Selenium.
    - Inspiration:
        Some of the interesting questions (tasks) which can be performed on this dataset -
        1. Determining the correlation between the market value of company respect to the public opinion of that company.
        2. Sentiment Analysis of the companies with a time series in a graph and reasoning the possible declines and rises.
        3. Evaluating troll users who try to occupy the social agenda.
2. [Stock Market Data (NASDAQ, NYSE, S&P500)](https://www.kaggle.com/paultimothymooney/stock-market-data)
    - Context: Daily stock market prices.
    - Content: 
        Date, Volume, High, Low, and Closing Price (for all NASDAQ, S&P500, and NYSE listed companies). Updated weekly.
    - Acknowledgements
        Banner Photo: https://unsplash.com/photos/amLfrL8LGls
3. Our dataset (after preprocessing)
    - Numeric:
        - [csv](https://drive.google.com/file/d/1n-lnVE84t9vrFdRpp5xdXo-T2m9uuUs4/view?usp=sharing)
        - [pkl](https://drive.google.com/file/d/1K9LQcBLn-w_f9LJKd7TansVPKk0pCs5B/view?usp=sharing)
    - Tweet:
        - [csv](https://drive.google.com/file/d/17kRqgl4rsskmZpPLgiDeF2mPojzN97Ox/view?usp=sharing)
