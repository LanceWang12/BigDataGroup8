#!/usr/bin/env python
# coding: utf-8


# importing libraries and packages
import os
import time
import numpy as np
import pandas as pd
from datetime import date
import snscrape.modules.twitter as sntwitter
from tqdm import tqdm, trange
from datetime import datetime
from datetime import timedelta
from threading import Thread 


# Write companies to an array
with open('./data/company.txt') as f:
    companies_abbs = f.readlines()
    companies_abbs = np.array([x[:-1] for x in companies_abbs])
    print(companies_abbs)

# Write companies' abbreviation to an array
with open('./data/companies.txt') as f:
    companies = f.readlines()
    companies = np.array([x[:-1] for x in companies])
    print(companies)

# Concatenate two arrays
companies = np.array((companies,companies_abbs))


# Crawl tweets by restricting daily tweet number

# Creating array to concatenate tweet data
def crawl_company_tweet_to_csv(first_date='2010-01-01',total_days=4383,company_list=companies,tweets_per_day=100):
    
    for idx,company in enumerate(companies[1]):
        
        print('company: ',company)
        tweets_array = np.zeros((0,3))
        
        for day in tqdm(range(total_days)): # 4383
            
            # Using TwitterSearchScraper to scrape data and append tweets to list
            since_date = datetime.strptime(first_date, '%Y-%m-%d') + timedelta(days=day)
            since_date = since_date.strftime('%Y-%m-%d')
            until_date = datetime.strptime(first_date, '%Y-%m-%d') + timedelta(days=day+1)
            until_date = until_date.strftime('%Y-%m-%d')

            for i,tweet in enumerate(sntwitter.TwitterSearchScraper(f'{company} stock OR price lang:en since:{since_date} until:{until_date}').get_items()): #filter:has_engagement
                
                if i>int(tweets_per_day): #number of tweets you want to scrape
                    break
                    
                single_tweet_array = np.array([[tweet.date,tweet.content,str(companies[0][idx])]])
                tweets_array = np.concatenate( ( tweets_array, single_tweet_array) )
                
        # Creating a dataframe from the tweets list above
        tweets_df = pd.DataFrame(tweets_array, columns=['Datetime','Text','Company'])
        tweets_df['Datetime'] = pd.to_datetime(tweets_df['Datetime']).dt.strftime('%Y-%m-%d')
        tweets_df.to_csv(f'./tweet_csv(20)/{companies[0][idx]}_tweets.csv', index=False)
        time.sleep(2)

        
# # Start to crawl tweet data
# crawl_company_tweet_to_csv(first_date='2010-01-01',total_days=4383,company_list=companies,tweets_per_day=20)

# Start to crawl tweet data

if __name__ == '__main__':

    print('Start to crawl !')
    # create and configure a new thread to run a function
    thread = Thread(target=crawl_company_tweet_to_csv)
    # start the task in a new thread
    thread.start()
    # display a message
    print('Waiting for the new thread to finish...')
    # wait for the task to complete
    thread.join()
    time.sleep(2)
